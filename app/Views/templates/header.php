<!doctype html>
<html>
<head>
    <title><?= esc($title) ?></title>
    <meta charset="utf-8">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />

</head>
<body class="bg-dark">
    
    <nav class="navbar navbar-primary bg-primary text-light">
    <?php if (!isset($_SESSION['user'])) : ?>
        <p></p>
        <button class="navbar-toggler" data-bs-toggle="modal" data-bs-target="#exampleModal"><i class="fa fa-user"></i></button>
    </nav>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header text-dark">
                    <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                    <button class="navbar-toggler text-dark" data-bs-dismiss="modal" aria-label="Close"><i class="fa fa-times text-dark"></i></button>
                </div>
                
                <div class="modal-body">
                <form action="/user/login" method="post">
                    <?= \Config\Services::validation()->listErrors() ?>

                    <?= csrf_field() ?>
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email"><br>

                    <label for="senha">Senha</label>
                    <input type="password" class="form-control" name="senha"><br>
                </div>

                <div class="modal-footer">
                    <a class="btn btn-primary" href="/user/register">Cadastrar</a>
                    <input type="submit" class="btn btn-success" value="Entrar">
                </form>
                </div>
                
            </div>
        </div>
    </div>
    <?php else: ?>
        <p></p>
        <p class="m-0 p-0"> <?= $_SESSION['user']['nome'] ?>
            <a class="ml-2 btn btn-danger" href="/user/logout">Sair</a>
        </p>
        
    </nav>
    <?php endif; ?>

    <div class="container bg-light text-center my-5 p-5 d-flex flex-column justify-content-center shadow">
        <h1 class="mb-5"><?= esc($title) ?></h1>