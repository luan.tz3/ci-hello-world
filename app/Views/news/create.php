<?= \Config\Services::validation()->listErrors() ?>

<form action="/news/create" method="post">
    <?= csrf_field() ?>
    <label for="title">Title</label>
    <input type="input" class="form-control" name="title"><br>

    <label for="body">Body</label>
    <textarea name="body" class="form-control"></textarea>

    <input type="submit" class="btn btn-primary mt-3" value="Publicar notícia">
</form>