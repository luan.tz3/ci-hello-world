<ul class="mx-auto px-5">

<?php
    if(isset($success))
    {
        echo '<div class="alert alert-success">'.esc($success).'</div>';
    }

    foreach($news as $n){
        echo ''
        .'<li class="mt-2"><a href="/news/'.esc($n['slug']).'">'.esc($n['title']).'</a>'
            .'<a href="/news/update/'.esc($n['slug']).'" class="ml-2">'
                .'<button class="btn btn-success btn-sm"><i class="fa fa-edit"></i></button>'
            .'</a>'
            .'<a href="/news/delete/'.esc($n['slug']).'" class="ml-2">'
                .'<button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>'
            .'</a>'
        .'</li>';
    }

?>

</ul>