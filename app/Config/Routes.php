<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

// Usuarios
$routes->get('user/logout', 'User::logout');
$routes->match(['get', 'post'], 'user/login', 'User::login');
$routes->match(['get', 'post'], 'user/register', 'User::register');
$routes->match(['get', 'post'], 'user/update/(:any)', 'User::update/$1');
$routes->get('user/delete/(:any)', 'User::delete/$1');
$routes->get('user', 'User::index');
$routes->get('user/(:any)', 'User::view/$1');

// News
$routes->match(['get', 'post'], 'news/create', 'News::create');
$routes->match(['get', 'post'], 'news/update/(:any)', 'News::update/$1');
$routes->get('news/delete/(:any)', 'News::delete/$1');
$routes->get('news', 'News::index');
$routes->get('news/(:any)', 'News::view/$1');

// Pages
$routes->get('/', 'Pages::index');
$routes->get('(:any)', 'Pages::view/$1');


/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
