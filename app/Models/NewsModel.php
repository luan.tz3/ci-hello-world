<?php

namespace App\Models;

use CodeIgniter\Model;

class NewsModel extends Model
{
    protected $table = 'news';
    protected $allowedFields = ['title', 'slug', 'body'];

    public function getNews($slug = false)
    {
        if($slug === false)
        {
            return $this->findAll();
        }
        
        return $this->asArray()->where(['slug' => $slug])->first();
    }
    public function createNews($request)
    {
        $this->save([
            'title' => $request->getPost('title'),
            'slug' => url_title($request->getPost('title'), '-', TRUE),
            'body' => $request->getPost('body')
        ]);
    }
    public function updateNews($id, $request)
    {   
        $this->update($id, [
            'title' => $request->getPost('title'),
            'slug' => url_title($request->getPost('title'), '-', TRUE),
            'body' => $request->getPost('body')
        ]);
    }
    public function deleteNews($id)
    {
        $this->delete($id);
    }
}