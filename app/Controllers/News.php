<?php

namespace App\Controllers;

use App\Models\NewsModel;

class News extends BaseController
{
	protected $helpers = ['url'];

	public function index()
	{
		$model = new NewsModel();

		if ($this->session->getFlashdata('success'))
		{
			$data['success'] = $this->session->getFlashdata('success');
		}

		$data['news'] = $model->getNews();
		$data['title'] = 'News';

		echo view('templates/header', $data);
		echo view('news/index', $data);
		echo view('templates/footer');
	}
	public function view($slug = null)
	{
		$model = new NewsModel();

		$data['news'] = $model->getNews($slug);

		if($data['news'] === null)
		{
            throw new \CodeIgniter\Exceptions\PageNotFoundException($slug);
		}
		$data['title'] = $data['news']['title'];

		echo view('templates/header', $data);
		echo view('news/view', $data);
		echo view('templates/footer');
	}
	public function create()
	{
		$model = new NewsModel();
		
		if	($this->request->getMethod() === 'post' && $this->validate([
				'title' => 'required|min_length[3]|max_length[127]',
				'body' => 'required'
			]))
		{
			$model->createNews($this->request);
			
			$this->session->setFlashdata('success', 'A notícia foi criada com sucesso!');
			return redirect()->route("news");
		}else
		{
			$data['title'] = 'Publicar Notícia';

			echo view('templates/header', $data);
			echo view('news/create');
			echo view('templates/footer');
		}
	}
	public function update($slug = false)
	{
		$model = new NewsModel();

		if($this->request->getMethod() === 'post' && $this->validate([
			'title' => 'required|min_length[3]|max_length[127]',
			'body' => 'required'
		]))
		{	
			$news = $model->getNews($slug);
			$id = $news['id'];
			$model->updateNews($id, $this->request);
				
			$this->session->setFlashdata('success', 'A notícia foi atualizada com sucesso!');
			return redirect()->route("news");
		}else
		{
			$data['news'] = $model->getNews($slug);
			if($data['news'] === null)
			{
				throw new \CodeIgniter\Exceptions\PageNotFoundException($slug);
			}

			$data['title'] = $data['news']['title'];
			
			echo view('templates/header', $data);
			echo view('news/update', $data);
			echo view('templates/footer');
		}
	}
	public function delete($slug = false)
	{
		$model = new NewsModel();
		$news = $model->getNews($slug);

		$model->deleteNews($news['id']);

		$this->session->setFlashdata('success', 'A notícia foi deletada com sucesso!');
		return redirect()->route('news');
	}
}
