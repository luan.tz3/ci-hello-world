<?php

namespace App\Controllers;

use CodeIgniter\Controller;

class Pages extends Controller
{
    public function index()
    {
        $directory = scandir(APPPATH.'Views/pages/');
        $files = array_filter($directory, function ($item) {
            return !is_dir(APPPATH.'Views/pages/'.$item);
        });
        $pages = [];
        foreach($files as $file){
            array_push($pages, substr($file, 0, -4));
        }

        $data['title'] = 'Welcome to ci-hello-world!';
        $data['pages'] = $pages;
        
        echo phpinfo();
        echo view('templates/header', $data);
        echo view('pages/welcome', $data);
        echo view('templates/footer');
    }

    public function view($page = 'home')
    {
        if ( !is_file(APPPATH.'Views/pages/'.$page.'.php') || $page === 'welcome') {
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }

        $data['title'] = ucfirst($page);

        echo view('templates/header', $data);
        echo view('pages/'.$page, $data);
        echo view('templates/footer');
    }
}