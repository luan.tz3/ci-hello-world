CREATE DATABASE cihelloworld CHARACTER SET utf8 COLLATE utf8_general_ci;
USE cihelloworld;

CREATE TABLE news(
    id int(11) NOT NULL AUTO_INCREMENT,
    title varchar(128) NOT NULL,
    slug varchar(128) NOT NULL,
    body text NOT NULL,
    PRIMARY KEY (id),
    KEY slug (slug)
);
INSERT INTO news VALUES
/*ID TITLE            SLUG            BODY */
(1,'Elvis sighted','elvis-sighted','Elvis was sighted at the Podunk internet cafe. It looked like he was writing a CodeIgniter app.'),
(2,"Say it isn't so!",'say-it-isnt-so','Scientists conclude that some programmers have a sense of humor.'),
(3,'Caffeination, Yes!','caffeination-yes',"World's largest coffee shop open onsite nested coffee shop for staff only.");

CREATE TABLE user(
    id int(11) NOT NULL AUTO_INCREMENT,
    nome varchar(128) NOT NULL,
    email varchar(256) NOT NULL,
    senha varchar(128) NOT NULL,
    acesso int NOT NULL,
    PRIMARY KEY (id),
    KEY email (email)
);
/* 1 | Luan    | luan@mail.com    | eE2S93wEjvdi6 */
INSERT INTO user (nome, email, senha, acesso) VALUES ('Luan', 'luan@mail.com', 'eE2S93wEjvdi6', 2);
